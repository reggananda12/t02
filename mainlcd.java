package tugas1;

public class mainlcd {
    public static void main(String[] args) {
        
        lcd displaylcd = new lcd();

        displaylcd.turnoff();

        displaylcd.setvolume(75);
        displaylcd.volumeup();
        displaylcd.volumedown();

        displaylcd.setbrightness(80);
        displaylcd.brightnessdown();
        displaylcd.brightnessup();

        displaylcd.setcable();
        displaylcd.cabledown();
        displaylcd.cableup();

        displaylcd.info();
    
}

}
