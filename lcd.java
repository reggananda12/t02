package tugas1;

    public class lcd {

        private String status;
        private int volume;
        private int brightness;
        private String cable;

        private int cableOption=1;

    
        public void turnoff(){
            this.status = "LCD Freeze";
        }
    
        public void turnon(){
            this.status = "LCD Menyala";
        }
    
        public void volumeup(){
            this.volume += 3;
        }
    
        public void volumedown(){
            this.volume -= 3;
        }
    
        public void setvolume(int volume){
            this.volume = volume;
        }
    
        public void brightnessdown(){
            this.brightness -= 3;
        }
    
        public void brightnessup(){
            this.brightness +=3;
        }
    
        public void setbrightness(int brightness){
            this.brightness = brightness;
        }
    
        public void cableup(){
            this.cableOption++;
        }
    
        public void cabledown(){
            this.cableOption--;
        }
    
        public void setcable(){
            switch (cableOption){
                case 1:
                 cable = "DVI";
                 break;
                case 2:
                 cable = "HDMI";
                 break;
                case 3:
                 cable = "VGA";
                 break;
                 default:
                cable = "Pilihan cable tidak tersedia";
                 break;
            }
        }    
        public void setcable(String cable){
            this.cable = cable;
        } 

        public void info(){
            System.out.println("===================LCD===================");
            System.out.println("Status LCD saat ini      = "+status);
            System.out.println("Volume LCD saat ini      = "+volume);
            System.out.println("Birghtness LCD saat ini  = "+brightness);
            System.out.println("Cable LCD saat ini       = "+cable);
        }
}
